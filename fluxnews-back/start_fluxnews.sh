#!/bin/sh
fuser -k 8090/tcp

nohup java -Dspring.profiles.active=$1 -Djasypt.encryptor.password=$2 -jar /home/debian/fluxnews/fluxnews-back/fluxnews-$3-SNAPSHOT.jar > fluxnews.log &