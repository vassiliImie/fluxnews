package fr.campus.fluxnews.ws;

import fr.campus.fluxnews.dto.ReportDto;
import fr.campus.fluxnews.entity.Report;
import fr.campus.fluxnews.service.report.impl.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("reports")
public class ReportWs {

    @Autowired
    private final ReportService reportService;

    public ReportWs(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/getAll")
    public List<Report> allReports(){return reportService.findAll();}

    @PostMapping("/addOne")
    public void react(@RequestBody ReportDto newReport){reportService.saveOrUpdate(newReport);}

    @PutMapping("/replaceOne")
    public void replaceReports(@RequestBody ReportDto newReport){ reportService.saveOrUpdate(newReport);}

    @GetMapping("/getOne/{id}")
    public Report singeReport(@PathVariable Long id) {return reportService.findOne(id);}

    @DeleteMapping("/deleteOne/{id}")
    public void deleteReport(@PathVariable Long id) {reportService.deleteById(id);}
}
