package fr.campus.fluxnews.ws;

import fr.campus.fluxnews.dto.CategoryDto;
import fr.campus.fluxnews.entity.Category;
import fr.campus.fluxnews.service.category.impl.CategoryService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("categories")
public class CategoryWs {

    private final CategoryService categoryService;

    CategoryWs(CategoryService categoryService){
        this.categoryService = categoryService;
    }

    @GetMapping("/getAll")
    public List<Category> allCategories(){
        return categoryService.findAll();
    }

    @PostMapping("/addOne")
    public void newCategory(@RequestBody CategoryDto newCategory){
        categoryService.saveOrUpdate(newCategory);
    }

    @PutMapping("/replaceOne/{id}")
    public void replaceCategory(@RequestBody CategoryDto newCategory) {
        categoryService.saveOrUpdate(newCategory);
    }

    @GetMapping("/getOne/{id}")
    public Category singleCategory(@PathVariable Long id) {
        return categoryService.findOne(id);
    }

    @DeleteMapping("/deleteOne/{id}")
    public void deleteCategories(@PathVariable Long id) {
        categoryService.deleteById(id);
    }
}
