package fr.campus.fluxnews.ws;

import fr.campus.fluxnews.dto.UserDto;
import fr.campus.fluxnews.entity.User;
import fr.campus.fluxnews.service.user.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
public class UserWs {

    @Autowired
    private final UserService userService;

    public UserWs(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/getAll")
    public List<User> allUsers(){return userService.findAll();}

    @PostMapping("/addOne")
    public void user(@RequestBody UserDto newUser){userService.saveOrUpdate(newUser);}

    @PutMapping("/replaceOne")
    public void replaceUser(@RequestBody UserDto newUser){userService.saveOrUpdate(newUser);}

    @GetMapping("/getOne/{id}")
    public User singleUser(@PathVariable Long id) {return userService.findOne(id);}

    @DeleteMapping("/deleteOne/{id}")
    public void deleteUser(@PathVariable Long id) {userService.deleteById(id);}
}
