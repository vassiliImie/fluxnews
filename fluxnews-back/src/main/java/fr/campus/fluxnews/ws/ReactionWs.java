package fr.campus.fluxnews.ws;

import fr.campus.fluxnews.dto.ReactionDto;
import fr.campus.fluxnews.entity.Reaction;
import fr.campus.fluxnews.service.reaction.impl.ReactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("reactions")
public class ReactionWs {

    @Autowired
    private final ReactionService reactionService;

    public ReactionWs(ReactionService reactionService) {
        this.reactionService = reactionService;
    }

    @GetMapping("/getAll")
    public List<Reaction> allReactions(){
        return reactionService.findAll();
    }

    @PostMapping("/addOne")
    public void react(@RequestBody ReactionDto newReaction){
        reactionService.saveOrUpdate(newReaction);
    }

    @PutMapping("/replaceOne")
    public void replaceReaction(@RequestBody ReactionDto newReaction) { reactionService.saveOrUpdate(newReaction); }

    @GetMapping("/getOne/{id}")
    public Reaction singleReaction(@PathVariable Long id) { return reactionService.findOne(id); }

    @DeleteMapping("/DeleteOne/{id}")
    public void deleteReactions(@PathVariable Long id) {
        reactionService.deleteById(id);
    }
}
