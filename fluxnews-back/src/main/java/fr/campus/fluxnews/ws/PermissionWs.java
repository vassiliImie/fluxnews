package fr.campus.fluxnews.ws;

import fr.campus.fluxnews.dto.PermissionDto;
import fr.campus.fluxnews.entity.Permission;
import fr.campus.fluxnews.service.permission.impl.PermissionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("permissions")
public class PermissionWs {

    private final PermissionService permissionService;

    PermissionWs(PermissionService permissionService){
        this.permissionService = permissionService;
    }

    @GetMapping("/getAll")
    public List<Permission> allPermissions(){
        return permissionService.findAll();
    }

    @PostMapping("/addOne")
    public void newPermission(@RequestBody PermissionDto newPermission){
        permissionService.saveOrUpdate(newPermission);
    }

    @PutMapping("/replaceOne")
    public void replacePermission(@RequestBody PermissionDto newPermission) {
        permissionService.saveOrUpdate(newPermission);
    }

    @GetMapping("/getOne/{id}")
    public Permission singlePermission(@PathVariable Long id) {
        return permissionService.findOne(id);
    }

    @DeleteMapping("/deleteOne/{id}")
    public void deletePermission(@PathVariable Long id) {
        permissionService.deleteById(id);
    }


}
