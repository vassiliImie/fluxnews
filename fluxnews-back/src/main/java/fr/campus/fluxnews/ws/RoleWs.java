package fr.campus.fluxnews.ws;

import fr.campus.fluxnews.dto.RoleDto;
import fr.campus.fluxnews.entity.Role;
import fr.campus.fluxnews.service.role.impl.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("roles")
public class RoleWs {

    @Autowired
    private final RoleService roleService;

    public RoleWs(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping("/getAll")
    public List<Role> allRoles(){return roleService.findAll();}

    @PostMapping("/addOne")
    public void roles(@RequestBody RoleDto newRole){ roleService.saveOrUpdate(newRole);}

    @PutMapping("/replaceOne")
    public void replaceRole(@RequestBody RoleDto newRole){roleService.saveOrUpdate(newRole);}

    @GetMapping("/getOne/{id}")
    public Role singleRole(@PathVariable Long id) {return roleService.findOne(id);}

    @DeleteMapping("/deleteOne/{id}")
    public void deleteRole(@PathVariable Long id) {roleService.deleteById(id);}
}
