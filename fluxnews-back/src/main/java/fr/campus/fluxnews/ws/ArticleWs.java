package fr.campus.fluxnews.ws;

import fr.campus.fluxnews.dto.ArticleDto;
import fr.campus.fluxnews.entity.Article;
import fr.campus.fluxnews.service.article.impl.ArticleService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("articles")
public class ArticleWs {

    private final ArticleService articleService;

    ArticleWs(ArticleService articleService){
        this.articleService = articleService;
    }

    @GetMapping("/getAll")
    public List<Article> allArticles(){
        return articleService.findAll();
    }

    @PostMapping("/addOne")
    public void newArticle(@RequestBody ArticleDto newArticle){
        articleService.saveOrUpdate(newArticle);
    }

    @PutMapping("/replaceOne")
    public void replaceArticle(@RequestBody ArticleDto newArticle) {
        articleService.saveOrUpdate(newArticle);
    }

    @GetMapping("/getOne/{id}")
    public Article singleArticle(@PathVariable Long id) {
        return articleService.findOne(id);
    }

    @DeleteMapping("/deleteOne/{id}")
    public void deleteArticle(@PathVariable Long id) {
        articleService.deleteById(id);
    }


}
