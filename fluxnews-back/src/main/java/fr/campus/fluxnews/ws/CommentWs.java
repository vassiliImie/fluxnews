package fr.campus.fluxnews.ws;

import fr.campus.fluxnews.dto.CommentDto;
import fr.campus.fluxnews.entity.Comment;
import fr.campus.fluxnews.service.comment.impl.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("comments")
public class CommentWs {

    @Autowired
    private final CommentService commentService;

    public CommentWs(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/getAll")
    public List<Comment> allComments(){
        return commentService.findAll();
    }

    @PostMapping("/addOne")
    public void addOne(@RequestBody CommentDto newComment){
        commentService.saveOrUpdate(newComment);
    }

    @PutMapping("/replaceOne")
    public void replaceComment(@RequestBody CommentDto newComment) {
        commentService.saveOrUpdate(newComment);
    }

    @GetMapping("/getOne/{id}")
    public Comment singleComment(@PathVariable Long id) {
        return commentService.findOne(id);
    }

    @DeleteMapping("/deleteOne/{id}")
    public void deleteComments(@PathVariable Long id) {
        commentService.deleteById(id);
    }
}
