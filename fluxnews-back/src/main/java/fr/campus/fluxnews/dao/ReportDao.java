package fr.campus.fluxnews.dao;

import fr.campus.fluxnews.entity.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportDao extends JpaRepository<Report,Long> {
}
