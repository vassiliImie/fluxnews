package fr.campus.fluxnews.dao;

import fr.campus.fluxnews.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryDao extends JpaRepository<Category,Long> {
}
