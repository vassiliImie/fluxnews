package fr.campus.fluxnews.dao;

import fr.campus.fluxnews.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionDao extends JpaRepository<Permission,Long> {
}
