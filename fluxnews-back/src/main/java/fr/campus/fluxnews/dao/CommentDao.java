package fr.campus.fluxnews.dao;

import fr.campus.fluxnews.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentDao extends JpaRepository<Comment,Long> {
}
