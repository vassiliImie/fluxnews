package fr.campus.fluxnews.dao;

import fr.campus.fluxnews.entity.Reaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReactionDao extends JpaRepository<Reaction,Long> {
}
