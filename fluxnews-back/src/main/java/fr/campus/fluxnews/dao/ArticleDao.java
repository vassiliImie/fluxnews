package fr.campus.fluxnews.dao;

import fr.campus.fluxnews.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleDao extends JpaRepository<Article,Long> {
}
