package fr.campus.fluxnews.dao;

import fr.campus.fluxnews.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role,Long> {
}
