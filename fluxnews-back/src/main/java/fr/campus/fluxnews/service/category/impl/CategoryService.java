package fr.campus.fluxnews.service.category.impl;

import fr.campus.fluxnews.dao.CategoryDao;
import fr.campus.fluxnews.dto.CategoryDto;
import fr.campus.fluxnews.entity.Category;
import fr.campus.fluxnews.service.base.ServiceBase;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CategoryService implements ServiceBase<Category, CategoryDto> {
    
    @Autowired
    final CategoryDao categoryDao;
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public CategoryService(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public List<Category> findAll() {
        log.info("Retourne tous les utilisateurs");
        return categoryDao.findAll();
    }

    @Override
    public Category findOne(Long id) {
        log.info("Retourne une category {}",id);
        return categoryDao.findById(id).orElse(null);
    }

    @Override
    public void saveOrUpdate(CategoryDto o) {
        log.info("Enregistrement d'un category");
        categoryDao.save(new ModelMapper().map(o,Category.class));
    }

    @Override
    public void delete(Category o) {
        deleteById(o.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Supprimer enregistrement category {}",id);
        categoryDao.deleteById(id);
    }
}
