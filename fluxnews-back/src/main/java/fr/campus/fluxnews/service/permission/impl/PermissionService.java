package fr.campus.fluxnews.service.permission.impl;

import fr.campus.fluxnews.dao.PermissionDao;
import fr.campus.fluxnews.dto.PermissionDto;
import fr.campus.fluxnews.entity.Permission;
import fr.campus.fluxnews.service.base.ServiceBase;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionService implements ServiceBase<Permission, PermissionDto> {

    @Autowired
    final PermissionDao permissionDao;
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public PermissionService(PermissionDao permissionDao) {
        this.permissionDao = permissionDao;
    }

    @Override
    public List<Permission> findAll() {
        log.info("Retourne tous les utilisateurs");
        return permissionDao.findAll();
    }

    @Override
    public Permission findOne(Long id) {
        log.info("Retourne une permission {}",id);
        return permissionDao.findById(id).orElse(null);
    }

    @Override
    public void saveOrUpdate(PermissionDto o) {
        log.info("Enregistrement d'un permission");
        permissionDao.save(new ModelMapper().map(o,Permission.class));
    }

    @Override
    public void delete(Permission o) {
        deleteById(o.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Supprimer enregistrement permission {}",id);
        permissionDao.deleteById(id);
    }
}
