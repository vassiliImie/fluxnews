package fr.campus.fluxnews.service.user.impl;

import fr.campus.fluxnews.dao.UserDao;
import fr.campus.fluxnews.dto.UserDto;
import fr.campus.fluxnews.entity.User;
import fr.campus.fluxnews.service.base.ServiceBase;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class UserService implements ServiceBase<User, UserDto> {

    @Autowired
    final UserDao userDao;

    final Logger log = LoggerFactory.getLogger(this.getClass());

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<User> findAll() {
        log.info("Retourne tous les utilisateurs");
        return userDao.findAll();
    }

    @Override
    public User findOne(Long id) {
        log.info("Retourne une user {}",id);
        return userDao.findById(id).orElse(null);
    }

    @Override
    public void saveOrUpdate(UserDto o) {
        log.info("Enregistrement d'un user");
        userDao.save(new ModelMapper().map(o,User.class));
    }

    @Override
    public void delete(User o) {
        deleteById(o.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Supprimer enregistrement user {}",id);
        userDao.deleteById(id);
    }
}
