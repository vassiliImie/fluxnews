package fr.campus.fluxnews.service.role.impl;

import fr.campus.fluxnews.dao.RoleDao;
import fr.campus.fluxnews.dto.RoleDto;
import fr.campus.fluxnews.entity.Role;
import fr.campus.fluxnews.service.base.ServiceBase;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService implements ServiceBase<Role, RoleDto> {

    @Autowired
    final RoleDao roleDao;
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public RoleService(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public List<Role> findAll() {
        log.info("Retourne tous les utilisateurs");
        return roleDao.findAll();
    }

    @Override
    public Role findOne(Long id) {
        log.info("Retourne une role {}",id);
        return roleDao.findById(id).orElse(null);
    }

    @Override
    public void saveOrUpdate(RoleDto o) {
        log.info("Enregistrement d'un role");
        roleDao.save(new ModelMapper().map(o,Role.class));
    }

    @Override
    public void delete(Role o) {
        deleteById(o.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Supprimer enregistrement role {}",id);
        roleDao.deleteById(id);
    }
}
