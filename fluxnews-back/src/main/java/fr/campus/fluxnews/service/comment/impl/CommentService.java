package fr.campus.fluxnews.service.comment.impl;

import fr.campus.fluxnews.dao.CommentDao;
import fr.campus.fluxnews.dto.CommentDto;
import fr.campus.fluxnews.entity.Comment;
import fr.campus.fluxnews.service.base.ServiceBase;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService implements ServiceBase<Comment, CommentDto> {

    @Autowired
    final CommentDao commentDao;
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public CommentService(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    @Override
    public List<Comment> findAll() {
        log.info("Retourne tous les utilisateurs");
        return commentDao.findAll();
    }

    @Override
    public Comment findOne(Long id) {
        log.info("Retourne une comment {}",id);
        return commentDao.findById(id).orElse(null);
    }

    @Override
    public void saveOrUpdate(CommentDto o) {
        log.info("Enregistrement d'un comment");
        commentDao.save(new ModelMapper().map(o,Comment.class));
    }

    @Override
    public void delete(Comment o) {
        deleteById(o.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Supprimer enregistrement comment {}",id);
        commentDao.deleteById(id);
    }
}
