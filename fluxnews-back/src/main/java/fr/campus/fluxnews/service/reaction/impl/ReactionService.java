package fr.campus.fluxnews.service.reaction.impl;

import fr.campus.fluxnews.dao.ReactionDao;
import fr.campus.fluxnews.dto.ReactionDto;
import fr.campus.fluxnews.entity.Reaction;
import fr.campus.fluxnews.service.base.ServiceBase;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReactionService implements ServiceBase<Reaction, ReactionDto> {

    @Autowired
    final ReactionDao reactionDao;
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public ReactionService(ReactionDao reactionDao) {
        this.reactionDao = reactionDao;
    }

    @Override
    public List<Reaction> findAll() {
        log.info("Retourne tous les utilisateurs");
        return reactionDao.findAll();
    }

    @Override
    public Reaction findOne(Long id) {
        log.info("Retourne une reaction {}",id);
        return reactionDao.findById(id).orElse(null);
    }

    @Override
    public void saveOrUpdate(ReactionDto o) {
        log.info("Enregistrement d'un reaction");
        reactionDao.save(new ModelMapper().map(o,Reaction.class));
    }

    @Override
    public void delete(Reaction o) {
        deleteById(o.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Supprimer enregistrement reaction {}",id);
        reactionDao.deleteById(id);
    }
}
