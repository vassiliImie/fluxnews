package fr.campus.fluxnews.service.report.impl;

import fr.campus.fluxnews.dao.ReportDao;
import fr.campus.fluxnews.dto.ReportDto;
import fr.campus.fluxnews.entity.Report;
import fr.campus.fluxnews.service.base.ServiceBase;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService implements ServiceBase<Report, ReportDto> {

    @Autowired
    final ReportDao reportDao;
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public ReportService(ReportDao reportDao) {
        this.reportDao = reportDao;
    }

    @Override
    public List<Report> findAll() {
        log.info("Retourne tous les utilisateurs");
        return reportDao.findAll();
    }

    @Override
    public Report findOne(Long id) {
        log.info("Retourne une report {}",id);
        return reportDao.findById(id).orElse(null);
    }

    @Override
    public void saveOrUpdate(ReportDto o) {
        log.info("Enregistrement d'un report");
        reportDao.save(new ModelMapper().map(o,Report.class));
    }

    @Override
    public void delete(Report o) {
        deleteById(o.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Supprimer enregistrement report {}",id);
        reportDao.deleteById(id);
    }
}
