package fr.campus.fluxnews.service.base;

import java.util.List;

/**
 * Interface définissant le crud pour les services
 */
public interface ServiceBase<T,U> {

    /**
     * Retourne toute les enregistrement
     */
    List<T> findAll();

    /**
     * Retourne un enregistement selon un id
     */
    T findOne(Long id);

    /**
     * Enregistre ou met à jour un enregistrement
     */
    void saveOrUpdate(U o);

    /**
     * Supprime un enregistrement
     */
    void delete(T o);

    /**
     * Supprime un enregistrement selon un id
     */
    void deleteById(Long id);
}
