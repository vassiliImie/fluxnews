package fr.campus.fluxnews.service.article.impl;

import fr.campus.fluxnews.dao.ArticleDao;
import fr.campus.fluxnews.dto.ArticleDto;
import fr.campus.fluxnews.entity.Article;
import fr.campus.fluxnews.service.base.ServiceBase;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class ArticleService implements ServiceBase<Article,ArticleDto> {

    @Autowired
    final ArticleDao articleDao;
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public ArticleService(ArticleDao articleDao) {
        this.articleDao = articleDao;
    }

    @Override
    public List<Article> findAll() {
        log.info("Retourne tous les utilisateurs");
        return articleDao.findAll();
    }

    @Override
    public Article findOne(Long id) {
        log.info("Retourne une article {}",id);
        return articleDao.findById(id).orElse(null);
    }

    @Override
    public void saveOrUpdate(ArticleDto o) {
        log.info("Enregistrement d'un article");
        articleDao.save(new ModelMapper().map(o,Article.class));
    }

    @Override
    public void delete(Article o) {
        deleteById(o.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Supprimer enregistrement article {}",id);
        articleDao.deleteById(id);
    }
}
