package fr.campus.fluxnews;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableEncryptableProperties
@SpringBootApplication
public class FluxnewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FluxnewsApplication.class, args);
	}

}
