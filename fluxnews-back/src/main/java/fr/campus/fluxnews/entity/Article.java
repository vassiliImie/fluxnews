package fr.campus.fluxnews.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
public class Article {

    @Id
    @GeneratedValue
    private Long id;

    private String url;
    private boolean display;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<User> users;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Report> report;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Comment> comments;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Reaction> reactions;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Category category;
}
