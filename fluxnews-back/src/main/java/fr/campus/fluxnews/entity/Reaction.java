package fr.campus.fluxnews.entity;

import fr.campus.fluxnews.enumeration.EnumReaction;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@ToString
@Entity
public class Reaction {

    @Id
    @GeneratedValue
    private Long id;

    private EnumReaction libelle;
    private Date creationTime;

    @OneToOne(cascade = CascadeType.ALL,fetch =FetchType.LAZY )
    private User user;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Article article;
}
