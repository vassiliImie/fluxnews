package fr.campus.fluxnews.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
public class Comment {

    @Id
    @GeneratedValue
    private Long id;

    private String libelle;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Article article;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private User user;
}
