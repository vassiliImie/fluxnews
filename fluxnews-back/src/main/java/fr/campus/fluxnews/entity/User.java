package fr.campus.fluxnews.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;
    private String lastName;
    private String nickName;
    private String email;
    private String password;
    private Date registration;
    private Date banishment;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Comment> comments;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Article> articles;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Report> reports;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Role role;
}
