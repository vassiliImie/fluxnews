package fr.campus.fluxnews.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
public class Category {

    @Id
    @GeneratedValue
    private Long id;

    private String libelle;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Article> articles;
}
