package fr.campus.fluxnews.enumeration;

import lombok.Getter;

/**
 * Enumeration des réaction que peuvent laisser un utilisateur sur un article
 *
 * Enumeration disposant d'un libelle
 */
@Getter
public enum EnumReaction {

    ADORE("Adore"),
    AIME("Aimer"),
    PAS_INTERESSANT("Pas intéressant"),
    DETESTE("Déteste");

    private String libelle;

    EnumReaction(String libelle) {
        this.libelle = libelle;
    }

}
