package fr.campus.fluxnews.dto;

import fr.campus.fluxnews.entity.Permission;
import fr.campus.fluxnews.entity.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Classe dto pour les roles
 */
@Getter
@Setter
@ToString
public class RoleDto {
    private Long id;
    private String libelle;
    private List<Permission> permissions;
    private User user;
}
