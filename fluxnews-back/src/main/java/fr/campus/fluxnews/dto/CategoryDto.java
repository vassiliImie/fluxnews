package fr.campus.fluxnews.dto;

import fr.campus.fluxnews.entity.Article;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Classe dto pour les category
 */
@Getter
@Setter
@ToString
public class CategoryDto {
    private Long id;
    private String libelle;
    private List<Article> articles;
}
