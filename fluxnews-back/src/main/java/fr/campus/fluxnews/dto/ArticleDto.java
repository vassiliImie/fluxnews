package fr.campus.fluxnews.dto;

import fr.campus.fluxnews.entity.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

/**
 * Classe dto pour les articles
 */
@Getter
@Setter
@ToString
public class ArticleDto {

    private Long id;
    private String url;
    private boolean display;
    private List<User> users;
    private List<Report> report;
    private List<Comment> comments;
    private List<Reaction> reactions;
    private Category category;
}
