package fr.campus.fluxnews.dto;

import fr.campus.fluxnews.entity.Article;
import fr.campus.fluxnews.entity.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Classe dto pour les report
 */
@Getter
@Setter
@ToString
public class ReportDto {
    private Long id;
    private String reason;
    private String object;
    private User user;
    private Article article;
}
