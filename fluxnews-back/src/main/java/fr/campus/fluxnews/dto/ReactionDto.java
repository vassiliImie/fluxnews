package fr.campus.fluxnews.dto;

import fr.campus.fluxnews.entity.Article;
import fr.campus.fluxnews.entity.User;
import fr.campus.fluxnews.enumeration.EnumReaction;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

/**
 * Classe dto pour les reactions
 */
@Getter
@Setter
@ToString
public class ReactionDto {
    private Long id;
    private EnumReaction libelle;
    private Date creationTime;
    private User user;
    private Article article;
}
