package fr.campus.fluxnews.dto;

import fr.campus.fluxnews.entity.Article;
import fr.campus.fluxnews.entity.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Classe dto pour les commentaires
 */
@Getter
@Setter
@ToString
public class CommentDto {
    private Long id;
    private String libelle;
    private Article article;
    private User user;
}
