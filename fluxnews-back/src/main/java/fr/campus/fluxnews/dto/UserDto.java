package fr.campus.fluxnews.dto;

import fr.campus.fluxnews.entity.Article;
import fr.campus.fluxnews.entity.Comment;
import fr.campus.fluxnews.entity.Report;
import fr.campus.fluxnews.entity.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Classe dto pour les users
 */
@Getter
@Setter
@ToString
public class UserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String nickName;
    private String email;
    private String password;
    private Date registration;
    private Date banishment;
    private List<Comment> comments;
    private List<Article> articles;
    private List<Report> reports;
    private Role role;
}
