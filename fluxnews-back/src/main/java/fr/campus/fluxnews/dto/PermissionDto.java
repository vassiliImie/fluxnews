package fr.campus.fluxnews.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Classe dto pour les permissions
 */
@Getter
@Setter
@ToString
public class PermissionDto {
    private Long id;
    private String libelle;
}
