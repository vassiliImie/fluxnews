import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { NavbarComponent } from './component/module/navbar/navbar.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ModalAuthentificationComponent } from './component/module/modal-authentification/modal-authentification.component';
import { HomepageComponent } from './component/pages/homepage/homepage.component';
import { FooterComponent } from './component/module/footer/footer.component';
import { ContactComponent } from './component/pages/contact/contact.component';
import { AboutComponent } from './component/pages/about/about.component';
import { InscriptionComponent } from './component/pages/inscription/inscription.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ModalAuthentificationComponent,
    HomepageComponent,
    NavbarComponent,
    FooterComponent,
    ContactComponent,
    AboutComponent,
    InscriptionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
