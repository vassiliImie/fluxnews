import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ModalAuthentificationComponent} from '../modal-authentification/modal-authentification.component'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  applicationName = "Fluxnews";
  
  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {
  }

  open(){
    this.modalService.open(ModalAuthentificationComponent,{ centered: true });
  }

}
