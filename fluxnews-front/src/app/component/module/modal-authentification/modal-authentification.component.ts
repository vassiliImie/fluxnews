import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserModel } from '../../../modele/UserModele'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-modal-authentification',
  templateUrl: './modal-authentification.component.html',
  styleUrls: ['./modal-authentification.component.css']
})
export class ModalAuthentificationComponent implements OnInit {

  /**
   * Variable
   */
  title = "Authentification";

  formAuthentification: FormGroup;

  constructor(private fb: FormBuilder, private http: HttpClient,public modal: NgbActiveModal) { }


  ngOnInit(): void {
    this.formAuthentification = this.fb.group({
      pseudo: [],
      mdp: []
    });
  }

  authentification() {
  }
}
