import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../../../validator/MustMatch'
import { empty } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { UserModel } from 'src/app/modele/UserModele';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  formUser: FormGroup;
  user: UserModel;

  constructor(private fb: FormBuilder, private http: HttpClient) { 
    this.user = new UserModel();
  }

  ngOnInit(): void {
    this.formUser = this.fb.group({
      pseudo: [],
      nom: [],
      prenom: [],
      mdp: [],
      confirmMdp: ['', [Validators.required]]
    }, { validator: MustMatch('mdp', 'confirmMdp') });
  }

  reset() {
    this.formUser.reset();
  }

  get f() {
    return this.formUser.controls;
  }

  onSubmit() {
    let values = this.formUser.value;
    let url = 'http://localhost:8080/users/addOne';
    console.log(this.user);
    this.user.firstName = values.pseudo;
    this.user.lastName = values.nom;
    this.user.nickName = values.prenom;
    this.user.password = values.mdp;

    console.log(this.user);
    this.http.post<UserModel>(url, this.user).subscribe({
      error: error => console.error('There was an error!', error)
      });
  }

}
