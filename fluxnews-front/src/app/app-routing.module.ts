import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarComponent } from './component/module/navbar/navbar.component';
import { ContactComponent } from './component/pages/contact/contact.component';
import { AboutComponent } from './component/pages/about/about.component';
import { HomepageComponent } from './component/pages/homepage/homepage.component';
import { InscriptionComponent } from './component/pages/inscription/inscription.component';

const routes: Routes = [
  { path: '', component: HomepageComponent},
  { path: 'navbar', component: NavbarComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'about', component: AboutComponent},
  { path: 'inscription', component: InscriptionComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
