# Fluxnews

A notre époque, le besoin d’informations est constant, particulièrement celles concernant l’actualité dans le monde, qu’elles soient politiques, éducatives, préventives comme en cette période de COVID-19. Notre proposition d’application répondrait à ce besoin d’être informé sur l’actualité, de permettre à nos utilisateurs de se renseigner à l’aide de sources variés ainsi que d’instaurer un esprit de communauté autour de notre outil afin de permettre un partage d’informations supplémentaire entre utilisateurs, dans le cadre de notre application mais également sur leurs réseaux sociaux afin de toujours plus pousser ce partage, répandre l’information et acquérir de la visibilité auprès d’un plus grand nombres de personnes.

L’actualité est un domaine intergénérationnel, touchant toutes personnes. Vous aimez le sport, on bien la politique ou vous êtes tout simplement curieux de ce qui se passe dans le monde, alors vous allez vouloir vous renseigner, trouver l’information. Notre application répond à ce besoin mais on vous offrant en plus de cette expérience, la possibilité de vous exprimer et de participer à une communauté. Nous ne faisons pas que répondre à un besoin, nous offrons une nouvelle expérience à l’utilisateur.

# Installation Angular

    - Installer Node.js : https://nodejs.org/en/
    - Installer Angular Cli pour commande : `npm install angular-cli`
    - Réaliser un update :  `npm update`
    - Builder le projet : `npm build`
    - Lancer le serveur : `ng serve`
    
    En cas d'erreur @angular/core not found faire un :
    - npm install --save-dev @angular-devkit/build-angular
        
# Installation Java
    
    - Installer le JDK 8 : https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

# Installation Gradle

    - Installer Gradle : https://gradle.org/
    - Build : `gradle build`