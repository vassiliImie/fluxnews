export SSHPASS=$1
echo $2
echo $3
echo $4

echo $5

echo $6

sshpass -p $1 ssh -o StrictHostKeyChecking=no $2@$3 rm -f /home/debian/fluxnews/fluxnews-back/*.jar

sshpass -p $1 ssh -o StrictHostKeyChecking=no $2@$3 rm -f /home/debian/fluxnews/fluxnews-back/*.sh

sshpass -e scp -o stricthostkeychecking=no -r fluxnews-back/build/libs/fluxnews-$6-SNAPSHOT.jar $2@$3:/home/debian/fluxnews/fluxnews-back/fluxnews-$6-SNAPSHOT.jar

sshpass -e scp -o stricthostkeychecking=no -r fluxnews-back/start_fluxnews.sh $2@$3:/home/debian/fluxnews/fluxnews-back/start_fluxnews.sh

sshpass -p $1 ssh -o StrictHostKeyChecking=no $2@$3 chmod +x /home/debian/fluxnews/fluxnews-back/start_fluxnews.sh

sshpass -p $1 ssh -o StrictHostKeyChecking=no $2@$3 /home/debian/fluxnews/fluxnews-back/start_fluxnews.sh $4 $5 $6


sshpass -e scp -o stricthostkeychecking=no -r fluxnews-front/dist/* $2@$3:/var/www/
sshpass -p $1 ssh -o StrictHostKeyChecking=no $2@$3 sudo nginx -s reload
